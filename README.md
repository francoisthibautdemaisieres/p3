# Table des matières

1. [Tâches](#Tâches)
2. [Documents](#Documents)
3. [Description Générale](#Description)

# Tâches

- [X] Formation des groupes
- [X] Inscription au groupe B04
- [X] Réalisation du cahier des charges & planning
- [ ] Apprendre git
- [ ] Conception et programmation de l'interface utilisateur
- [ ] Conception et programmation du chiffrement du message
- [ ] Conception et programmation des types d'actions utilisé pour déchiffré le message
- [ ] Phase de test
- [ ] Preparation de la démonstration du programme
- [ ] Réalisation du rapport en LaTex
- [ ] Présentation du projet
- [ ] Soumission final du projet

# Documents

- [Cahier des charge](https://uclouvain-my.sharepoint.com/:w:/g/personal/stefan_tasnadi_student_uclouvain_be/EW-cZy6fmwZPqVE6KVDKP3ABqEFTXMism0KMzAV3SqZ0RA)
- [Planning](https://uclouvain-my.sharepoint.com/:x:/g/personal/francois_thibaut_student_uclouvain_be/EeM0EaBe_wFOpeZN7ETprVEB08F3LET4abjnON1l0VjxOA?e=fJ6sfX)
- [Présentation](https://uclouvain-my.sharepoint.com/:p:/g/personal/francois_thibaut_student_uclouvain_be/EScqAalEV-FAolzixQVrNXwBMbiSLptBp56praJO6bc-6Q?e=901NIl)

# Description

## Votre mission

Félicitations, votre équipe vient d’être fraîchement recrutée au service de la Sûreté de l’État belge. Prenant modèle sur le célèbre MI5 de sa majesté (la Reine d’Angleterre), le service de la Sûreté de samajesté (le Roi des Belges) vous a chargé de développer les outils technologiques les plus ingénieux et les plus inattendus pour équiper son meilleur agent secret, l’agent 2121. James Bond zéro-zéro-sept a à sa disposition des montres-laser ou des stylos explosifs, l’agent deux-cent-douze ne doit pas être en reste! Le premier besoin d’un agent secret est de pouvoir transporter des informations de manière confidentielle, comme par exemple le code d’une installation nucléaire secrète ou le numéro du portail duchâteau de Laeken. Votre première réalisation va utiliser le meilleur de la technologie spatiale embarquée pour réaliser un coffre-à-message inviolable, le **MagicLock**. Cet équipement facile à transporter et à transmettre entre agents doit permettre d’entrer un message secret et de le chiffrer en utilisant un code secret. Le code secret permet ensuite de retrouver le message secret. L’innovation majeure du MagicLock est que le code secret n’est pas un mot de passe sous forme de chiffres et de lettres, mais un ensemble de mouvements à réaliser dans l’espace et d’interactions avec l’appareil lui-même. Ceci permet de garantir que seul l’agent en possession physique du MagicLock est en mesure de récupérer le message.

## Calendrier

<div aligne="center">
    <img src="https://github.com/francoistm/Projet_en_informatique/blob/main/P3/calandar.png"/>
</div>

## Objectifs

Les objectifs méthodologiques sont d’améliorer la capacité des étudiant·e·s à concevoir et réaliser un projet en groupe, de plus grande envergure et complexité que les deux projets précédents. Les connaissances acquises au cours des projets P1 et P2 (concevoir un cahier des charges, un échéancier, rédiger un rapport, concevoir une présentation, utiliser LATEX) seront utilisées pour le projet P3. **Les objectifs de formation en informatique sont d’apprendre à réaliser un programme pour une plateforme embarquée, de concevoir une interface utilisateur avec des capacités d’affichage limitées, et de comprendre les principes de la cryptographie**. Par ailleurs, les étudiant·e·s apprennent à utiliser un gestionnaire de version et l’utilisent lors de la réalisation de leur projet.

## Matériel

Le projet P3 est réalisé sur une plateforme de type Raspberry PI. Le Raspberry PI3 est un mini-ordinateur de la taille d’une carte de crédit, mais qui offre de très grandes possibilités. Il a été conçu pour l’apprentissage de la programmation, mais ses utilisations vont maintenant bien au delà ! Le langage python est le langage le plus utilisé pour programmer sur Raspberry PI.

En plus du Raspberry PI, le projet utilise la carte d’extension **Sense Hat**. Cette carte d’extension offre un grand nombre de capteurs et de périphériques :

- Un écran de 8x8 pixels LED couleur ;
- Un joystick supportant les quatre directions et la validation (bouton poussoir) ;
- De nombreux capteurs : Gyroscope, accéléromètre, magnétomètre, capteur de température,capteur de pression atmosphérique, et capteur d’humidité.

Le Sense Hat est déployé dans la station spatiale internationale dans le cadre du projet Astro PI5.Chacun des jeunes lauréats du concours Astro PI a vu son code fonctionner sur le Sense Hat de lastation pendant 3 rotations autour de la terre en mai 2019.

## Programmation et simulateur

L’accès aux fonctionnalités du Sense Hat est facilité par une librairie, disponible par défaut sur les Raspberry PI fournis. La description de cette librairie et de ses fonctions est disponible ici : 

[https://pythonhosted.org/sense-hat/api](https://pythonhosted.org/sense-hat/api)

Étant donné les circonstances actuelles, il ne sera pas possible de réaliser le projet sur des machines physiques. Afin de permettre à tous les membres du groupe de travailler de leur côté sur le programme,nous utiliserons un simulateur de Raspberry PI et Sense Hat, disponible à l’adresse suivante :

[https://trinket.io/sense-ha](https://trinket.io/sense-ha)

## Description du projet

La première étape de votre projet va être de rédiger un cahier des charges et un planning pour sa réalisation. Vous mettrez plus tard en place le dépôt git qui sera utilisé pour le travail collaboratif :Un cours spécifique sera consacré à cette outil en S11. Votre MagicLock devra répondre, au minimum, aux spécifications suivantes :

- Il proposera une interface graphique utilisant l’affichage LED et le joystick du SenseHat :
  1. Si aucun message codé n’est enregistré, elle doit permettre d’enregistrer un nouveau message, puis le nouveau code.
  2. Si un message codé est déjà enregistré, elle permet de tenter d’entrer le code, et d’afficher le message si celui-ci est correct.
  3. Une fois le message affiché, il peut être détruit si souhaité (retour au point 1), ou conservé(retour au point 2).
- Le message est composé d’une suite de chiffres de 0 à 9, entrés directement sur le MagicLocken utilisant l’interface.
- Le code est une suite de positions du Raspberry PI et du Sense Hat dans l’espace : appareil posé à plat, tourné de 90 degrés vers la droite, la gauche, etc. et ce dans les 3 dimensions del’espace. On ne considèrera que les configurations perpendiculaires aux axes x, y et z (donc pasd’orientation à 45 degrés). Chaque position dans la séquence est validée par un appui sur le bouton poussoir.
- Le message et le code doivent être conservés sous formes de fichiers, et être de nouveau disponibles en cas d’extinction du Raspberry PI et de remise sous tension.
- Dans un premier temps le message et le code seront conservés en  clair dans les fichiers les stockant. Dans un second temps, le message sera conservé sous forme chiffrée, et le code seraconservé sous forme hachée.

Il est bien entendu possible d’ajouter des fonctionnalités ou des objectifs supplémentaires ! Vous avezplus de liberté pour choisir les caractéristiques de votre MagicLock que pour les deux projets précédents : les idées innovantes ou les “petits plus” par rapport aux autres projets, seront certainement unatout pour être sélectionné, en plus d’une réalisation sans faille des spécifications minimales attendues.

Le MagicLock doit fonctionner sur le simulateur Raspberry PI et être l’objet d’une démonstration lorsde la présentation finale.

## Livrables

Les documents et travaux suivants seront à rendre au cours du projet :

- Le cahier des charges et le planning rédigé pour chaque groupe est **à rendre sur Moodle pour le 20 novembre 2020 à 18h**. Le cahier des charges (max 3 pages) doit être rédigé en utilisant un traitement de texte et le planning réalisé en utilisant un tableur. Ce dernier doit tenir et être lisible sur une page imprimée au format A4. Les deux documents doivent être rendus au format PDF.
- La présentation de chaque groupe, prévue pour une durée de 6 minutes, et réalisée avec un logiciel de présentation est **à rendre sur Moodle pour le 11 décembre 2020 à 18h**. Cette présentation fera au maximum 6 slides (titres compris) et sera rendue au format PDF. Elle est destinée à introduire la démonstration du projet réalisé, qui doit être prévue pour durer 6 minutes elle aussi. La présentation doit mettre l’emphase sur les aspects originaux du projet.
- Le projet (code réalisé et commenté, et rapport rédigé en LATEX) est **à rendre avant le 18 décembre 2019 à 18h sur Moodle**. Le rapport fera au maximum 4 pages en plus de la page de garde, avec une fonte de taille 11 minimum, et sera rendu au format PDF.

Le livrable final du projet P3 devra être une archive .zip et respectera scrupuleusement les contraintes de rendu suivantes, où AAA est le nom de votre groupe (par exemple, “B12”) :

- Un dossier nommé P3_AAA_Programme contenant l’ensemble de votre programme, c’est-à-dire tous vos fichiers .py, ainsi que ceux que nous vous avons fourni ;
- Un dossier nommé P3_AAA_Rapport dans lequel vous mettrez le rapport PDF sous le nom deP3_AAA_Rapport.pdf et ses sources LaTeX.

## Évaluation

L’évaluation du projet P3 porte sur la qualité du programme (3 points), la présentation et la démonstration (4 points), le rapport, le cahier des charges et le planning (2 points en tout), et la participation individuelle (1 point).

Il est impossible de soumettre son travail sur Moodle après l’heure fixée. Si un groupe d’étudiant·e·s n’a pas téléversé son travail sur Moodle avant l’heure fixée, chaque étudiant·e du groupe se verra attribuer la note de 0 pour le programme et/ou le rapport.

# Made in

<div aligne="center" width="50%">
    <img src="https://github.com/francoistm/Projet_en_informatique/blob/main/P3/uclouvain_logo.png"/>
</div>